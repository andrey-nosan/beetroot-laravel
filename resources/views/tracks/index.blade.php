<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table border="1">
    <tr>
        <th>Track ID</th>
        <th>Name</th>
        <th>Composer</th>
        <th>Milliseconds</th>
        <th>Bytes</th>
        <th>Price</th>
    </tr>

    @foreach($tracks as $track)
        <tr>
            <td><p>{{ $track['TrackId'] }}</p></td>
            <td><p>{{ $track['Name'] }}</p></td>
            <td><p>{{ $track['Composer'] ?? 'No composer' }}</p></td>
            <td><p>{{ $track['Milliseconds'] }}</p></td>
            <td><p>{{ $track['Bytes'] }}</p></td>
            <td><p>{{ $track['UnitPrice'] }}</p></td>
        </tr>

    @endforeach

</table>


</body>
</html>
