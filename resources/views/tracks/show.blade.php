<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table border="1">
    <tr>
        <td><p>Id</p></td>
        <td><p>{{ $track['TrackId'] }}</p></td>
    </tr>
    <tr>
        <td><p>Name</p></td>
        <td><p>{{ $track['Name'] }}</p></td>
    </tr>
    <tr>
        <td><p>Composer</p></td>
        <td><p>{{ $track['Composer'] ?? 'No composer' }}</p></td>
    </tr>
    <tr>
        <td><p>Milliseconds</p></td>
        <td><p>{{ $track['Milliseconds'] }}</p></td>
    </tr>
    <tr>
        <td><p>Bytes</p></td>
        <td><p>{{ $track['Bytes'] }}</p></td>
    </tr>
    <tr>
        <td><p>Price</p></td>
        <td><p>{{ $track['UnitPrice'] }}</p></td>
    </tr>

</table>


</body>
</html>
