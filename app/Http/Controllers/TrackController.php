<?php

namespace App\Http\Controllers;

use App\Models\Track;
use App\Services\TrackService;
use Illuminate\View\View;

class TrackController extends Controller
{
    private $trackService;

    public function __construct(TrackService $trackService)
    {
        $this->trackService = $trackService;
    }

    public function index(): View
    {
        $tracks = $this->trackService->getAllTracks();

        return view('tracks.index')->with(['tracks' => $tracks->toArray()]);
    }

    public function show(Track $track): View
    {
        return view('tracks.show')->with(['track' => $track->toArray()]);
    }
}
