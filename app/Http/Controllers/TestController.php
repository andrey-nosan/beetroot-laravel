<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        return '<h1>Test page</h1><p>' . __METHOD__ . '</p>';
    }
}
