<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        $cond = false;

        $sql = Category::query()
            ->select(['name', 'type'])
            ->where('age', '>=', 25)
            ->where(function ($query) use ($cond) {
                /** @var Builder $query */
                if ($cond) {
                    return $query->where('height', '>', 180)
                        ->orWhere('height', '<', 150);
                }

                return $query->whereBetween('height', [150, 180]);
            })
        ;

//        /** @var Category $category */
//        $category = $sql->first();

//        print_r($category->toArray());


//        echo $category->name;

        dump($sql->getBindings());
        dd($sql->toSql());

        $users = [
            ['id' => 1, 'name' => 'Bob'],
            ['id' => 2, 'name' => 'John'],
            ['id' => 3, 'name' => 'Alex'],
            ['id' => 4, 'name' => 'Bill'],
            ['id' => 5, 'name' => 'Barbara'],
        ];

        return response()->json($users);
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $users = [
            ['id' => 1, 'name' => 'Bob'],
            ['id' => 2, 'name' => 'John'],
            ['id' => 3, 'name' => 'Alex'],
            ['id' => 4, 'name' => 'Bill'],
            ['id' => 5, 'name' => 'Barbara'],
        ];

        $key = array_search($id, array_column($users, 'id'));

        if ($key === false) {
            return response()->json(
                [
                    'status' => false,
                    'message' => 'User not found',
                ],
                404
            );
        }

        $user = $users[$key];

        return response()->json($user);
    }

    public function test(int $test)
    {
        return $test;
    }
}
