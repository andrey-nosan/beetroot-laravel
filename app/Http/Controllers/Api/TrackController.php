<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Track;
use App\Services\TrackService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TrackController extends Controller
{
    private $trackService;

    public function __construct(TrackService $trackService)
    {
        $this->trackService = $trackService;
    }

    public function index(): JsonResponse
    {
        $tracks = $this->trackService->getAllTracks();

        return response()->json($tracks);
    }

    public function show(int $id): JsonResponse
    {
        $track = $this->trackService->getTrackById($id);

        return response()->json($track);
    }
}
