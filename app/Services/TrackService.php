<?php

namespace App\Services;

use App\Models\Track;
use Illuminate\Support\Collection;

class TrackService
{
    public function getAllTracks(): Collection
    {
        return Track::query()
            ->select(['TrackId', 'Name', 'Composer', 'Milliseconds', 'Bytes', 'UnitPrice'])
            ->limit(10)
            ->get();
    }

    /**
     * @param int $id
     * @return Track
     */
    public function getTrackById(int $id): Track
    {
        /** @var Track $track */
        $track = Track::query()
            ->select(['TrackId', 'Name', 'Composer', 'Milliseconds', 'Bytes', 'UnitPrice'])
            ->where('TrackId', $id)
            ->first();

        return $track;
    }
}
