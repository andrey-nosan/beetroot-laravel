<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TrackController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::get('/test', [TestController::class, 'index']);

//Route::get('/user', [UserController::class, 'index'])->name('user.index');
//Route::get('/user/{id}/{arg}/{arg3}', [UserController::class, 'test'])->name('user.test');
//Route::get('/user/{id}', [UserController::class, 'show'])->name('user.show');

//Route::get('/user/{user}/post/{post}');

Route::get('/test/{test}', [UserController::class, 'test'])->where('test', '[0-9]+');

Route::resource('/product', ProductController::class);

Route::resource('/user', UserController::class)
    ->only(['index', 'show'])
//    ->where('user', '[0-9]+')
;

//Route::resources([
//    '/product' => ProductController::class,
//    '/user' => UserController::class,
//]);

Route::get('/track', [TrackController::class, 'index'])->name('track.index');
Route::get('/track/{track}', [TrackController::class, 'show'])->name('track.show');
